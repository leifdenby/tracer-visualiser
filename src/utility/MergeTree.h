#pragma once

#include <vector>

enum MergeTreeType {None, Split, Join};

class MergeTree
{
  public:
    MergeTree();
    void setValues(const std::vector<std::vector<std::vector<float>>>&, const float);
    void computeJoinTree();
    void computeSplitTree();
    void simplifyMergeTree(float);
    std::vector<std::vector<std::vector<int>>> computeVisitedForIsovalue(float);

    MergeTreeType mergeTreeType = MergeTreeType::None;

    inline const bool isSplitTree() { return this->mergeTreeType == MergeTreeType::Split; }

    //
    // Returns the persistence pairs that include the isovalue
    //
    std::vector<int> getActiveComponents(float);

    void computeMergeTree();

    float nanValue;

    std::vector<std::tuple<int, int, int>> getAdjacent(int, int, int, int, int, int);

    // Just for convenience
    int xdim, ydim, zdim;
    // 3D field the merge tree is computed on
    const std::vector<std::vector<std::vector<float>>> *data;
    // Parent list in he emrge tree
    std::vector<int> mergeTree;
    // Arranged by <lowerIndex, higherIndex, persistence>
    std::vector<std::tuple<int, int, int>> persistencePairs;

    // The branch a vertex belongs to
    std::vector<std::vector<std::vector<int>>> vis;

    // Which of the branches have been simplified or not
    std::vector<bool> activeBranches;

    // Same as vis, but with a -1 for the used branches
    //std::vector<std::vector<std::vector<int>>> simplifiedVisited;
};
