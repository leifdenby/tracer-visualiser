#include <qpoint.h>
#ifdef __APPLE__
#include <GLUT/glut.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#endif

#include <QGLWidget>
#include <QKeyEvent>
#include <QtMath>
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <math.h>
#include <qnamespace.h>
#include <queue>
#include <stdlib.h> /* srand, rand */
#include <string>
#include <time.h> /* time */
#include <typeinfo>
#include <vector>

#include "./TracerVisualiserWidget.h"
#include "./TracerVisualiserWindow.h"
#include "./utility/Geometry.h"
#include "./utility/TetrahedronDepth.h"
#include "./utility/Utility.h"
#include "src/utility/MarchingCubes.h"
#include "src/utility/MeshTriangle.h"
#include "src/utility/SurfaceMesh.h"

using namespace std;
using namespace tv9k::utility;

void
TracerVisualiserWidget::setMaterial(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha, GLfloat shininess)
{
    glMaterialfv(GL_FRONT, GL_AMBIENT, &(vector<GLfloat>({ red, green, blue, alpha })[0]));
    glMaterialfv(GL_FRONT, GL_DIFFUSE, &(vector<GLfloat>({ red, green, blue, alpha })[0]));
    glMaterialfv(GL_FRONT, GL_SPECULAR, &(vector<GLfloat>({ red, green, blue, alpha })[0]));
    glMaterialf(GL_FRONT, GL_SHININESS, shininess);

    glMaterialfv(GL_BACK, GL_AMBIENT, &(vector<GLfloat>({ 0.5, .5, .5, alpha })[0]));
    glMaterialfv(GL_BACK, GL_DIFFUSE, &(vector<GLfloat>({ 0.5, .5, .5, alpha })[0]));
    glMaterialfv(GL_BACK, GL_SPECULAR, &(vector<GLfloat>({ .5, .5, .5, alpha })[0]));
    glMaterialf(GL_BACK, GL_SHININESS, shininess);
}

TracerVisualiserWidget::TracerVisualiserWidget(QWidget* parent,
                                               QWidget* _sibling,
                                               QWidget* _histogramSibling,
                                               Data* _data)
  : QGLWidget(parent)
{
    // Set points to other singletons
    this->data = _data;
    this->sibling = _sibling;
    this->histogramSibling = dynamic_cast<HistogramWidget*>(_histogramSibling);

    // Default values for paraters
    this->scale = data->zdim * 8;

    // Initialise Arcball
    Ball_Init(&theBall);
    Ball_Place(&theBall, qOne, 1);
}

void
TracerVisualiserWidget::computeFiberSurface()
{}

void
TracerVisualiserWidget::initializeGL()
{
    glClearColor(0.3, 0.3, 0.3, 0.0);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    // Enable alpha blending
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // Do not enable this, doesn't work properly
    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

    //glCullFace(GL_BACK);
    //glEnable(GL_CULL_FACE);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(90, 1, 0.01, 10000);
}

void
TracerVisualiserWidget::resizeGL(int w, int h)
{
    glViewport(0, 0, w, h);
}

void
TracerVisualiserWidget::generateDisplayList(const SurfaceMesh& mesh, const SurfaceType surfaceType)
{
    if (surfaceType == SurfaceType::isosurface) {
        glDeleteLists(displayListIndex, 1);
        displayListIndex = glGenLists(1);
        glNewList(displayListIndex, GL_COMPILE);

    } else if (surfaceType == SurfaceType::fibersurface) {
        glDeleteLists(displayListIndexF, 1);
        displayListIndexF = glGenLists(1);
        glNewList(displayListIndexF, GL_COMPILE);
    } else if (surfaceType == SurfaceType::combinedSurface) {
        glDeleteLists(displayListIndexC, 1);
        displayListIndexC = glGenLists(1);
        glNewList(displayListIndexC, GL_COMPILE);
    }
    else {
        // We shouldn't be here
        assert(false);
    }

    glBegin(GL_TRIANGLES);

    for (int i = 0; i < mesh.triangles.size(); i++) {

        double focusedOpacity;

        if (surfaceType == SurfaceType::isosurface) {
            focusedOpacity = this->isosurfaceOpacity;
        } else if (surfaceType == SurfaceType::fibersurface) {
            focusedOpacity = this->fibersurfaceOpacity;
        } else if (surfaceType == SurfaceType::combinedSurface) {
            focusedOpacity = this->cartesianSurfaceOpacity;
        }
        else {
            // We shouldn't be here
            assert(false);
        }

        const auto nonFocusedOpacity = 0.7;

        auto opacity = nonFocusedOpacity;

        // The statement isIsosurface == this->data->isIsoContourSelected holds when
        // 1. the selected object is from the isosurface and we are currently drawing the isosurface
        // 2. the selected object is from the fibersurface and we are currently drawing the fibersurface
        const bool areWeSelectedObject =
          (surfaceType == this->data->selectedSurfaceType[this->data->currentTimestep] &&
           mesh.triangles[i].triangleId == this->data->selectedID[this->data->currentTimestep]);

        // Full opacity if we're the selecte object
        if (
          // No selected object, so everything is selected
          (-1 == this->data->selectedID[this->data->currentTimestep]) ||
          // If we're drawing something not selected
          (true == areWeSelectedObject)) {
            opacity = focusedOpacity;
        }

        // Set colour (this is the ID of the object), +1 is to avoid (0, 0, 0) which
        // is ambiguous for both
        if (surfaceType == SurfaceType::isosurface) {
            if (mesh.triangles[i].triangleId > 255)
            {
                //cout << "It's bigger" << mesh.triangles[i].triangleId << endl;
            }
            //assert (mesh.triangles[i].triangleId < 255);
            glColor4ub((mesh.triangles[i].triangleId + 1) % 255, 0, 0, 255);
        } else if (surfaceType == SurfaceType::fibersurface) {
            glColor4ub(0, mesh.triangles[i].triangleId + 1, 0, 255);
        } else if (surfaceType == SurfaceType::combinedSurface) {
            glColor4ub(0, 0, mesh.triangles[i].triangleId + 1, 255);
        }

        // Set up the vertices and the normal of the triangle
        GLfloat vertices[3][3] = { mesh.triangles[i].vertices[0][0], mesh.triangles[i].vertices[0][1],
                                   mesh.triangles[i].vertices[0][2], mesh.triangles[i].vertices[1][0],
                                   mesh.triangles[i].vertices[1][1], mesh.triangles[i].vertices[1][2],
                                   mesh.triangles[i].vertices[2][0], mesh.triangles[i].vertices[2][1],
                                   mesh.triangles[i].vertices[2][2] };

        GLfloat normals[3][3] = { mesh.triangles[i].normals[0][0], mesh.triangles[i].normals[0][1],
                                  mesh.triangles[i].normals[0][2], mesh.triangles[i].normals[0][0],
                                  mesh.triangles[i].normals[1][1], mesh.triangles[i].normals[1][2],
                                  mesh.triangles[i].normals[0][0], mesh.triangles[i].normals[2][1],
                                  mesh.triangles[i].normals[2][2] };

        GLfloat normalsInv[3][3] = { -1*mesh.triangles[i].normals[0][0], -1*mesh.triangles[i].normals[0][1],
                                  -1*mesh.triangles[i].normals[0][2], -1*mesh.triangles[i].normals[0][0],
                                  -1*mesh.triangles[i].normals[1][1], -1*mesh.triangles[i].normals[1][2],
                                  -1*mesh.triangles[i].normals[0][0], -1*mesh.triangles[i].normals[2][1],
                                  -1*mesh.triangles[i].normals[2][2] };

        if (data->flatNormals) {
            drawSolidTriangle(vertices);
        } else {

            const auto plotWidget = dynamic_cast<PlotWidget*>(sibling);

            if (surfaceType == SurfaceType::isosurface && true == this->shouldIntersect &&
                plotWidget->isPointInsidePolygon(mesh.triangles[i].projectedVertices[0])) {
                setMaterial(1, 0.3, 0, opacity, 0.0);
            } else {
                setMaterial(
                  mesh.triangles[i].color[0], mesh.triangles[i].color[1], mesh.triangles[i].color[2], opacity, 100.0);
            }

            glNormal3fv(normals[0]);
            glVertex3fv(vertices[0]);

            if (surfaceType == SurfaceType::isosurface && true == this->shouldIntersect &&
                plotWidget->isPointInsidePolygon(mesh.triangles[i].projectedVertices[1])) {
                setMaterial(1, 0.3, 0, opacity, 0.0);
            } else {
                setMaterial(
                  mesh.triangles[i].color[0], mesh.triangles[i].color[1], mesh.triangles[i].color[2], opacity, 100.0);
            }

            glNormal3fv(normals[1]);
            glVertex3fv(vertices[1]);

            if (surfaceType == SurfaceType::isosurface && true == this->shouldIntersect &&
                plotWidget->isPointInsidePolygon(mesh.triangles[i].projectedVertices[2])) {
                setMaterial(1, 0.3, 0, opacity, 0.0);
            } else {
                setMaterial(
                  mesh.triangles[i].color[0], mesh.triangles[i].color[1], mesh.triangles[i].color[2], opacity, 100.0);
            }

            glNormal3fv(normals[2]);
            glVertex3fv(vertices[2]);
        }
    }
    glEnd();
    glEndList();
}

void
TracerVisualiserWidget::drawSolidTriangle(GLfloat vertices[3][3])
{
    GLfloat a[] = { vertices[1][0] - vertices[0][0], vertices[1][1] - vertices[0][1], vertices[1][2] - vertices[0][2] };

    GLfloat b[] = { vertices[2][0] - vertices[0][0], vertices[2][1] - vertices[0][1], vertices[2][2] - vertices[0][2] };

    // Invert Normal for sub/super level sets
    GLfloat normal[] = { isovalueMult * (a[2] * b[1] - b[1] * b[2]),
                         isovalueMult * (a[0] * b[2] - a[2] * b[0]),
                         isovalueMult * (a[1] * b[0] - a[0] * b[1]) };

    GLfloat normalFlipped[] = { -1 * normal[0], -1 * normal[1], -1 * normal[2] };

    glNormal3fv(normalFlipped);
    glVertex3fv(vertices[0]);
    glVertex3fv(vertices[1]);
    glVertex3fv(vertices[2]);
}

void
TracerVisualiserWidget::drawAxis(GLfloat length, GLfloat width)
{
    GLUquadric* line = gluNewQuadric();

    // Z Axis
    setMaterial(0., 0., 1., 1.0, 30.);
    gluCylinder(line, width, width, length, 100, 100);

    // Y Axis
    setMaterial(0., 1., 0., 1.0, 30.);
    glPushMatrix();
    glRotatef(-90, 1, 0, 0);
    gluCylinder(line, width, width, length, 100, 100);
    glPopMatrix();

    // X Axis
    setMaterial(1., 0., 0., 1.0, 30.);
    glPushMatrix();
    glRotatef(90, 0, 1, 0);
    gluCylinder(line, width, width, length, 100, 100);
    glPopMatrix();

    gluDeleteQuadric(line);
}

void
TracerVisualiserWidget::drawWiredCube(const GLfloat vertices[8][3])
{
    glBegin(GL_LINES);
    {
        // Front Side
        glVertex3fv(vertices[0]);
        glVertex3fv(vertices[2]);

        glVertex3fv(vertices[2]);
        glVertex3fv(vertices[3]);

        glVertex3fv(vertices[1]);
        glVertex3fv(vertices[3]);

        glVertex3fv(vertices[0]);
        glVertex3fv(vertices[1]);

        // Back Side
        glVertex3fv(vertices[4]);
        glVertex3fv(vertices[6]);

        glVertex3fv(vertices[6]);
        glVertex3fv(vertices[7]);

        glVertex3fv(vertices[5]);
        glVertex3fv(vertices[7]);

        glVertex3fv(vertices[4]);
        glVertex3fv(vertices[5]);

        // Sides
        glVertex3fv(vertices[0]);
        glVertex3fv(vertices[4]);

        glVertex3fv(vertices[2]);
        glVertex3fv(vertices[6]);

        glVertex3fv(vertices[3]);
        glVertex3fv(vertices[7]);

        glVertex3fv(vertices[1]);
        glVertex3fv(vertices[5]);
    }
    glEnd();
}

void
TracerVisualiserWidget::drawScene()
{
    glEnable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Rescale normals when scaling
    glEnable(GL_RESCALE_NORMAL);


    // This is set so that glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE); light ups both sides properly
    // @TODO Figure out why this even works.
    // I am not careful about setting the orientation of the triangles at all
    // Why are they even consistent with the min/max trees?
    if (this->data->isoField->mergeTrees->at(0).isSplitTree())
    {
        glFrontFace(GL_CCW);
    }
    else
    {
        glFrontFace(GL_CW);
    }

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    GLfloat light_pos[] = { 0, 0, 10000, 1. };
    glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
    glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, 180.);

    // Scrolling
    glTranslatef(0.0, 0.0, -1 * scale / 2);

    // Offset along x, y (with the right mouse button)
    glTranslatef(translateX, translateY, 0.0);

    // HVect vNow;
    // vNow.x = 50;
    // vNow.y = 100;

    // Ball_Mouse(&theBall, vNow);
    // Ball_Update(&theBall);

    // Arcball
    GLfloat mNow[16];
    Ball_Value(&theBall, mNow);
    glMultMatrixf(mNow);

    // GLUquadric* sphere = gluNewQuadric();
    // gluSphere(sphere, 2, 3, 3);

    // Moving Away from the center
    if (-1 == this->data->selectedID[this->data->currentTimestep] || false == this->shouldFocusSelectedObject) {
        // glTranslatef((-1.0 * this->data->xdim) / 2.0, (-1.0 * this->data->ydim) / 2.0, (1.0 * this->data->zdim)
        // / 2.0);
        glTranslatef((-1.0 * this->data->xdim) / 2.0, 0, 0);
        glTranslatef(0, 0, (1.0 * this->data->ydim) / 2.0);
        glTranslatef(0, (-1.0 * this->data->zdim) / 2.0, 0);
    } else {
        const vector<GLfloat> center = { (this->data->selectedObjectMinMax[this->data->currentTimestep].first[0] +
                                          this->data->selectedObjectMinMax[this->data->currentTimestep].second[0]) /
                                           2,
                                         (this->data->selectedObjectMinMax[this->data->currentTimestep].first[1] +
                                          this->data->selectedObjectMinMax[this->data->currentTimestep].second[1]) /
                                           2,
                                         (this->data->selectedObjectMinMax[this->data->currentTimestep].first[2] +
                                          this->data->selectedObjectMinMax[this->data->currentTimestep].second[2]) /
                                           2 };

        // Compute selected object center
        glTranslatef(-1.0 * center[0], -1.0 * center[1], -1.0 * center[2]);
    }

    // if (data->render3DLabels)
    //{
    // glPushMatrix();
    //{
    // GLfloat vertices[4][3] = {
    //{ 0., 0., 0. },
    //{ 1., 0., 0. },
    //{ 0., -1., 0. },
    //{ 1., -1., 0. },
    //};
    // GLfloat normal[3] = {
    // 0., 0., 1.
    //};

    // glPushMatrix();
    //{
    // setMaterial(1., 0.0, 0., 1., 1.);
    // glTranslatef(this->data->xdim, 0, 0);
    // glScalef(10, 10, 10);
    // glBegin(GL_POLYGON);
    //{

    // glNormal3fv(normal);
    // glVertex3fv(vertices[0]);
    // glVertex3fv(vertices[1]);
    // glVertex3fv(vertices[3]);
    // glVertex3fv(vertices[2]);
    //}
    // glEnd();
    //}
    // glPopMatrix();

    // glPushMatrix();
    //{
    // setMaterial(0., 1.0, 0., 1., 1.);

    // glRotatef(90., 0., 0., 1.);

    // glRotatef(190., 1., 0., 0.);

    // glTranslatef(this->data->ydim, 0, 0);
    // glScalef(10, 10, 10);

    // glBegin(GL_POLYGON);
    //{
    // GLfloat n[3] = { -1 * normal[0], -1 * normal[1], -1 * normal[2] };
    // glNormal3fv(n);
    // glVertex3fv(vertices[0]);
    // glVertex3fv(vertices[1]);
    // glVertex3fv(vertices[3]);
    // glVertex3fv(vertices[2]);
    //}
    // glEnd();
    //}
    // glPopMatrix();

    // glPushMatrix();
    //{
    // setMaterial(0., 0.0, 1., 1., 1.);

    // glRotatef(-90., 1., 0., 0.);
    // glRotatef(135., 0., 1., 0.);

    // glTranslatef(0, -this->data->zdim, 0);

    ////glRotatef(-135., 0., 0., 1.);

    // glScalef(10, 10, 10);
    // glBegin(GL_POLYGON);
    //{

    // glNormal3fv(normal);
    // glVertex3fv(vertices[0]);
    // glVertex3fv(vertices[1]);
    // glVertex3fv(vertices[3]);
    // glVertex3fv(vertices[2]);
    //}
    // glEnd();
    //}
    // glPopMatrix();
    //}
    // glPopMatrix();
    //}

    glRotatef(-90., 1., 0., 0.);

    this->drawAxis(1000., 1.0 * this->data->xdim / 800.0);

    // Bounding Cube
    glPushMatrix();
    {
        glScalef(this->data->xdim - 1, this->data->ydim - 1, this->data->zdim - 1);
        setMaterial(255, 255, 255, 100, 30.0);
        this->drawWiredCube(tv9k::geometry::cubeVertices);
    }
    glPopMatrix();

    // Draw cube around selected object
    glPushMatrix();
    {
        const vector<GLfloat> pointA = this->data->selectedObjectMinMax[this->data->currentTimestep].first;
        const vector<GLfloat> pointB = this->data->selectedObjectMinMax[this->data->currentTimestep].second;

        const GLfloat sideX = pointB[0] - pointA[0];
        const GLfloat sideY = pointB[1] - pointA[1];
        const GLfloat sideZ = pointB[2] - pointA[2];

        const GLfloat vertices[8][3] = { { pointA[0], pointA[1], pointA[2] },
                                         { pointA[0] + sideX, pointA[1], pointA[2] },
                                         { pointA[0], pointA[1] + sideY, pointA[2] },
                                         { pointA[0] + sideX, pointA[1] + sideY, pointA[2] },
                                         { pointA[0], pointA[1], pointA[2] + sideZ },
                                         { pointA[0] + sideX, pointA[1], pointA[2] + sideZ },
                                         { pointA[0], pointA[1] + sideY, pointA[2] + sideZ },
                                         { pointA[0] + sideX, pointA[1] + sideY, pointA[2] + sideZ } };

        setMaterial(255, 255, 255, 100, 30.0);
        this->drawWiredCube(vertices);
    }
    glPopMatrix();

    //for (int i = 0 ; i < this->data->xdim ; i++)
    //{
        //for (int j = 0 ; j < this->data->ydim ; j++)
        //{
            //for (int k = 0 ; k < this->data->zdim ; k++)
            //{
                //// Isosurface
                //glPushMatrix();
                //{
                    //glTranslatef(i, j, k);

                    //float r = 0;
                    //float g = 0;
                    //float b = 0;

                    //if (this->data->fibersurfaceMeshes[this->data->currentTimestep].triangles.size() > 0 && this->data->currentSignedDistanceField[this->data->currentTimestep][i][j][k] < 0)
                    //{
                        //r = 1.0;
                    //}

                    //if (this->data->isoField->values[this->data->currentTimestep][i][j][k] > this->data->isoField->currentIsovalue)
                    //{
                        //g = 1.0;
                    //}
                    //else
                    //{
                        ////r = 1;
                    //}

                    //if (r == 1 || g == 1)
                    //{
                        //setMaterial(r, g, b, 1., 30.0);
                    //}
                    //else
                    //{
                        //setMaterial(1, 1, 1, 0.5, 30.0);

                    //}
                    //GLUquadric* sphere = gluNewQuadric();
                    //gluSphere(sphere, 0.1, 5, 5);

                //}
                //glPopMatrix();
            //}
        //}
    //}

    // Combined Surface
    glPushMatrix();
    {
        if (false == shouldHideCombinedSurface) {
            glCallList(displayListIndexC);
        }
    }
    glPopMatrix();

    // Isosurface
    glPushMatrix();
    {
        if (false == shouldHideIsosurface) {
            glCallList(displayListIndex);
        }
    }
    glPopMatrix();

    // Fiber Surface
    glPushMatrix();
    {
        if (false == shouldHideFiberSurface) {
            glCallList(displayListIndexF);
        }
    }
    glPopMatrix();


    glFlush();
}

void
TracerVisualiserWidget::paintGL()
{
    this->drawScene();
}

void
TracerVisualiserWidget::wheelEvent(QWheelEvent* event)
{
    this->scale -= event->angleDelta().y() / 10;

    if (scale <= 0) {
        scale = 0;
    }

    this->update();
}

void
TracerVisualiserWidget::mousePressEvent(QMouseEvent* event)
{
    if (event->button() == Qt::LeftButton) {
        HVect vNow;
        vNow.x = (2.0 * event->x() - width()) / width();
        vNow.y = (height() - 2.0 * event->y()) / height();

        Ball_Mouse(&theBall, vNow);
        Ball_BeginDrag(&theBall);

        this->update();
    }
    if (event->button() == Qt::RightButton) {
        initialX = event->localPos().x();
        initialY = event->localPos().y();
    }
}

void
TracerVisualiserWidget::mouseMoveEvent(QMouseEvent* event)
{
    if (event->buttons() == Qt::LeftButton) {
        HVect vNow;
        vNow.x = (2.0 * event->localPos().x() - width()) / width();
        vNow.y = (height() - 2.0 * event->y()) / height();

        Ball_Mouse(&theBall, vNow);
        Ball_Update(&theBall);

        this->update();
    } else if (event->buttons() == Qt::RightButton) {
        float x = event->localPos().x();
        float y = event->localPos().y();

        translateX -= (initialX - x) / 10;
        translateY += (initialY - y) / 10;

        initialX = event->localPos().x();
        initialY = event->localPos().y();

        this->update();
    }
}

void
TracerVisualiserWidget::mouseReleaseEvent(QMouseEvent* event)
{
    if (event->button() == Qt::LeftButton) {
        Ball_EndDrag(&theBall);
        this->update();
    }
}

void
TracerVisualiserWidget::keyPressEvent(QKeyEvent* event)
{
    if (event->key() == Qt::Key_W) {
        translateX += 0.1 * this->data->xdim;
        this->update();
    }
    if (event->key() == Qt::Key_A) {
        translateY += 0.1 * this->data->xdim;
        this->update();
    }
    if (event->key() == Qt::Key_S) {
        translateX -= 0.1 * this->data->xdim;
        this->update();
    }
    if (event->key() == Qt::Key_D) {
        translateY -= 0.1 * this->data->xdim;
        this->update();
    }
}

void
TracerVisualiserWidget::mouseDoubleClickEvent(QMouseEvent* event)
{
    if (event->button() == Qt::LeftButton) {
        // Get the cursor location on the screen
#ifdef __APPLE__
        int xPos = event->x() * 2;
        int yPos = (height() - event->y()) * 2;
#else
        int xPos = event->x();
        int yPos = height() - event->y();
#endif

        // Render with solid colours (no lighting)
        glDisable(GL_LIGHTING);
        glDisable(GL_LIGHT0);
        this->drawScene();

        // Pick the solid colour at the cursor location
        struct
        {
            GLubyte red, green, blue, alpha;
        } pixel;
        glReadPixels(xPos, yPos, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, &pixel);

        // Rerender normally with lighting
        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);
        this->update();

        // Isosurfaces are red
        if (0 != pixel.red && 0 == pixel.green && 0 == pixel.blue) {

            this->data->selectedID[this->data->currentTimestep] = pixel.red - 1;

            cout << "Selected object Id is " << this->data->selectedID[this->data->currentTimestep] << endl;

            this->data->selectedObjectMinMax[this->data->currentTimestep] =
              this->data->isosurfaceMeshes[this->data->currentTimestep].getMinMaxPointsObject(
                this->data->selectedID[this->data->currentTimestep]);

            this->data->selectedSurfaceType[this->data->currentTimestep] = SurfaceType::isosurface;
        }
        // Fiber surfaces are blue
        else if (0 == pixel.red && 0 != pixel.green && 0 == pixel.blue) {

            this->data->selectedID[this->data->currentTimestep] = pixel.green - 1;

            this->data->selectedObjectMinMax[this->data->currentTimestep] =
              this->data->fibersurfaceMeshes[this->data->currentTimestep].getMinMaxPointsObject(
                this->data->selectedID[this->data->currentTimestep]);

            cout << "Selected object Id is " << this->data->selectedID[this->data->currentTimestep] << endl;

            // this->data->sceneCenter[this->data->currentTimestep] =
            // this->fibersurfaceMesh.getCenter(this->data->selectedID[this->data->currentTimestep]);
            //this->data->isIsoContourSelected[this->data->currentTimestep] = false;

            this->data->selectedSurfaceType[this->data->currentTimestep] = SurfaceType::fibersurface;
        }
        // TODO Add the combined Surface, it's true
        else {
            this->data->selectedID[this->data->currentTimestep] = -1;
            this->data->selectedObjectMinMax[this->data->currentTimestep] = { { 0, 0, 0 }, { 0, 0, 0 } };
            this->data->selectedSurfaceType[this->data->currentTimestep] = SurfaceType::none;
        }

        dynamic_cast<PlotWidget*>(sibling)->update();

        // Generate the display lists again
        generateDisplayList(this->data->isosurfaceMeshes[this->data->currentTimestep], SurfaceType::isosurface);
        generateDisplayList(this->data->fibersurfaceMeshes[this->data->currentTimestep], SurfaceType::fibersurface);
        this->update();

        // Print object dimensions
        const vector<GLfloat> pointA = this->data->selectedObjectMinMax[this->data->currentTimestep].first;
        const vector<GLfloat> pointB = this->data->selectedObjectMinMax[this->data->currentTimestep].second;

        const GLfloat sideX = pointB[0] - pointA[0];
        const GLfloat sideY = pointB[1] - pointA[1];
        const GLfloat sideZ = pointB[2] - pointA[2];

        printf("The selected object has dimensions (%f, %f, %f).\n", sideX, sideY, sideZ);

        // std::cout << "The coordinates are "<< xPos << " " << yPos <<" and the
        // colour of the pixel is " << (int)pixel.red << " " << (int)pixel.green << "
        // " << (int)pixel.blue << " and alpha " << (int)pixel.alpha << " and the ID
        // is " << this->data->selectedID[this->data->currentTimestep] << std::endl;
    }
    if (event->buttons() == Qt::RightButton) {
        translateX = 0.;
        translateY = 0.;
        this->update();
    }
}
